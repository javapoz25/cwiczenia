package com.sda.javapoz25.listy.Zad1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * Stwórz listę Integerów. Wykonaj zadania:
 *     - dodaj do listy 5 elementów ze scannera
 *     - dodaj do listy 5 elementów losowych
 *     - wypisz elementy
 * Sprawdź działanie aplikacji.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); // do pobierania wart. od. uż.
        Random generator = new Random();             // do losowania wart.

        List<Integer> list = new ArrayList<>();

        //- dodaj do listy 5 elementów ze scannera
        for (int i = 0; i < 5; i++) {
            System.out.println("Wpisz liczbę:");
            int pobranaWartosc = scanner.nextInt();
            list.add(pobranaWartosc);
        }

        //- dodaj do listy 5 elementów losowych (do 100)
        // (generowanie losowej liczby)
        for (int i = 0; i < 5; i++) {
            // generowanie liczby +/- zakres INTEGER
            // generator to obiekt generujący kolejno wartości
            // nextInt - losuj następną liczbę - (bound) z zakresu do 100 bez setki.
            int wygenerowanaWartosc = generator.nextInt(100);
            list.add(wygenerowanaWartosc);
        }

        // [ 5 wpisanych liczb, 5 losowanych liczb]
        System.out.println(list);
    }
}
