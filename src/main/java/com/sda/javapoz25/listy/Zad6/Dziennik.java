package com.sda.javapoz25.listy.Zad6;

import java.util.*;

/**
 * Klasa Dziennik powinna:
 */
public class Dziennik {

    // *     - posiadać (jako pole) listę Studentów.
    private List<Student> list = new ArrayList<>();

    public void dodajStudenta(Student student) {
        list.add(student);
    }

    //- posiadać metodę 'dodajStudenta(Student):void' do dodawania nowego studenta do dziennika
    public void dodajStudenta(String imie, String nazwisko, String numerIndeksu) {
        list.add(new Student(imie, nazwisko, numerIndeksu));
    }

    //    - posiadać metodę 'usuńStudenta(Student):void' do usuwania studenta
    public void usunStudenta(Student student) {
        list.remove(student);
    }

    //    - posiadać metodę 'usuńStudenta(String):void' do usuwania studenta po jego numerze indexu
    public void usunStudenta(String studentIndeks) {
        // powinniśmy unikać iteracji i usuwania w jednym obiegu foreach
        // Opcja 1:
//        for (Student student : list) {
//            if(student.getNumerIndeksu().equalsIgnoreCase(studentIndeks)){
//                list.remove(student); // jeśli tak usuwasz, to możliwe jest usunięcie max 1 elementu
//                break;
//            }
//        }
        // Opcja 2:
//        for (int i = 0; i < list.size(); i++) {
//            if (list.get(i).getNumerIndeksu().equalsIgnoreCase(studentIndeks)) {
//                list.remove(i);
//                i--; // cofamy się po usunięciu
//            }
//        }
        // Opcja 3:
        Iterator<Student> it = list.iterator();
        while (it.hasNext()) { // jeśli posiadasz element
            // każde wywołanie metody next, powoduje przskok do następnego el.
            Student element = it.next(); // to przeskocz do niego
            // błędem jest, robienie w obiegu (jednym, while) dwóch wywołań metody "next()"

            if (element.getNumerIndeksu().equalsIgnoreCase(studentIndeks)) {
                it.remove(); // usuń element, na który wskazywałeś/zwróciłeś
            }
        }
    }

    //    - posiadać metodę 'zwróćStudenta(String):Student' która jako parametr przyjmuje numer indexu studenta, a w wyniku zwraca konkretnego studenta.

    /**
     * Zwraca studenta, jeśli nie zwróci studenta, wartość zwrócona będzie równa <code>null</code>.
     *
     * @param indeks - indeks szukany
     * @return - null, lub znaleziony student o danym indeksie.
     */
    public Optional<Student> zwrocStudenta(String indeks) {
        for (Student student : list) {
            if (student.getNumerIndeksu().equalsIgnoreCase(indeks)) {
                // jeśli znalazłeś studenta o tym indeksie, to zwróć go
                return Optional.of(student);
            }
        }
        // przypadek, kiedy student o takim id nie znajduje się w kolekcji.
        return Optional.empty(); // wersja java 7

//         return null;
//         throw new StudentNotFound();
    }

    //    - posiadać metodę 'podajŚredniąStudenta(String):double' która przyjmuje indeks studenta i zwraca średnią ocen studenta.
    public Optional<Double> podajSredniaStudenta(String indeks) {
        Optional<Student> studentOptional = zwrocStudenta(indeks);
        if (studentOptional.isPresent()) {
            // obliczenie sredniej.
            Student student = studentOptional.get();

            return student.obliczSrednia();
        }
        // jeśli nie znajdę studenta ?
        return Optional.empty();
    }

    //    - posiadać metodę 'podajStudentówZagrożonych():List<Student>'
    public List<Student> podajStudentowZagrozonych() {
        List<Student> zagrozeni = new ArrayList<>();

        for (Student student : list) {
            Optional<Double> srednia = student.obliczSrednia();

            if (srednia.isPresent() && srednia.get() < 2.5) { // jeśli ocena jest niższa niż 2.5
                zagrozeni.add(student);
            } else if (!srednia.isPresent()) { // jeśli nie ma oceny
                zagrozeni.add(student); // zagrozony, ponieważ nie bedzie kasyfikowany.
            }
        }

        return zagrozeni;
    }

    // - posiadać metodę 'posortujStudentówPoIndeksie():List<Student>' - która sortuje listę studentów po numerach indeksów, a następnie zwraca posortowaną listę.
    public List<Student> posrotujPoIndeksie() {
        Collections.sort(list);

        return list;
    }

    //    - posiadać metodę ‘dodajOcene(String, Double):void
    public void dodajOcene(String indeks, Double ocena) {
        Optional<Student> studentOptional = zwrocStudenta(indeks);
        if (studentOptional.isPresent()) {
            Student student = studentOptional.get();

            student.getListaOcen().add(ocena);
        } else {
            // System.err - spowoduje wypisanie na czerwono (jest to wypisanie na standardowe wyjście błędu)
            System.err.println("Nie ma takiego studenta");
        }
    }

    public void wypiszDziennik() {
        for (Student student : list) {
            System.out.println(student);
        }

    }

}
