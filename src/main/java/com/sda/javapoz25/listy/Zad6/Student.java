package com.sda.javapoz25.listy.Zad6;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Stwórz aplikację, a w niej klasę Dziennik i Student. Klasa Student powinna:
 * - posiadać listę ocen studenta (List<Double>)
 * - posiadać (pole) numer indeksu studenta (String)
 * - posiadać (pole) imię studenta
 * - posiadać (pole) nazwisko studenta
 */
@Data
@EqualsAndHashCode
@NoArgsConstructor
public class Student implements Comparable<Student>{
    private String imie;
    private String nazwisko;
    private String numerIndeksu;

    private List<Double> listaOcen = new ArrayList<>();

    public Student(String imie, String nazwisko, String numerIndeksu) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.numerIndeksu = numerIndeksu;
    }

    public Optional<Double> obliczSrednia() {
        double suma = 0;
        for (Double aDouble : listaOcen) {
            suma += aDouble;
        }
        if(listaOcen.size() >0) {
            // jeśli są oceny (można dzielić przez liczbę)
            return Optional.of(suma/listaOcen.size());
        }else{
            return Optional.empty();
        }
    }

    // 1
    // 2
    // 11
    // 12
    // 13
    // 21
    // 22
    // 23
    @Override
    public int compareTo(Student other) {
        // dokonuje parsowania ze String -> Integer obu liczb/indeksów.
        return Integer.compare(Integer.parseInt(this.numerIndeksu), Integer.parseInt(other.numerIndeksu));
    }
}
