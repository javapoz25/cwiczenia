package com.sda.javapoz25.listy.Zad6;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Dziennik dziennik = new Dziennik();

        dziennik.dodajStudenta(new Student("Pawel", "Gawel", "123"));
        dziennik.dodajOcene("123", 3.2);
        dziennik.dodajOcene("123", 2.0);
        dziennik.dodajOcene("123", 2.1);
        dziennik.dodajOcene("123", 2.2);
        dziennik.dodajOcene("123", 1.9);

        dziennik.dodajStudenta(new Student("Adam", "Gawel", "124"));
        dziennik.dodajOcene("124", 5.0);
        dziennik.dodajOcene("124", 5.1);
        dziennik.dodajOcene("124", 3.2);
        dziennik.dodajOcene("124", 3.5);

        dziennik.dodajStudenta(new Student("Rafał", "Waldek", "127"));
        dziennik.dodajOcene("127", 3.0);

        dziennik.dodajStudenta(new Student("Marek", "Bogumil", "125"));

//        dziennik.usunStudenta("123"); // ilu usunie? - 2

//        Optional<Student> p = dziennik.zwrocStudenta("128");
//        if(p.isPresent()) {
//            Student znaleziony = p.get();
//
//            System.out.println(znaleziony.getImie());
//        }

        dziennik.posrotujPoIndeksie();
        dziennik.wypiszDziennik();

        List<Student> zagrozeni = dziennik.podajStudentowZagrozonych();
        System.out.println("Zagrozeni:");
        for (Student student : zagrozeni) {
            System.out.println(student);
        }


    }
}
