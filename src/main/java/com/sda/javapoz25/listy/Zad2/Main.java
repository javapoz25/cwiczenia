package com.sda.javapoz25.listy.Zad2;

import java.util.*;

/**
 * Stwórz oddzielnego maina, a w nim kolejną listę integerów. Wykonaj zadania:
 * - dodaj do listy 10 liczb losowych
 * - oblicz sumę elementów na liście (wypisz ją)
 * - oblicz średnią elementów na liście (wypisz ją)
 * - stwórz kopię listy i ją posortuj:  (Collections.sort( listaDoPosortowania) - sortuje listę)
 * (przykład użycia Collections.sort(lista):
 * List<Integer> liczby = new ArrayList<>();
 * liczby.add(5);
 * liczby.add(10);
 * liczby.add(15);
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); // do pobierania wart. od. uż.
        Random generator = new Random();             // do losowania wart.

        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < 11; i++) {
            int wygenerowanaWartosc = generator.nextInt(100);
            list.add(wygenerowanaWartosc);
        }
        System.out.println("Liczby : " + list);

        //########################################################## JAVA 7
        // klasyczne podejście (java 7)
//        int suma = 0;
        double suma = 0;
        for (Integer wartosc : list) {
            suma += wartosc;
        }
        // dokładność obliczeń zawsze zostanie zachowana/sprowadzona dla najbardziej precyzyjnego typu
        double srednia = suma / list.size();

        System.out.println("Srednia : " + srednia);
        System.out.println("Suma : " + suma);

        //########################################################## JAVA 8
        // podejście java 8 (+ stream)
        int sumaStream = list.stream()
                .mapToInt(value -> value)
                // od momentu, gdy użyjemy mapToInt, strumień ma więcej funkcji dot. wart. liczbowych
                .sum();
        System.out.println("Suma : " + sumaStream);

        // wynikiem nie jest liczba, tylko optional
        OptionalDouble optionalSrednia =
                list.stream()
                        .mapToInt(value -> value)
                        .average();

        if (optionalSrednia.isPresent()) {
            double wartoscSredniej = optionalSrednia.getAsDouble();

            System.out.println("Wynik: " + wartoscSredniej);
        }


        //*** KOPIOWANIE LISTY - kopiujemy elementy
        // użycie konstruktora kopiującego
        List<Integer> kopia = new ArrayList<>(list);
        Collections.sort(kopia);

        // jak posortuje? wartościami (rosnąco/malejąco)

        // kopia listy, posortowana
//        List<Integer> posortowana = kopia.stream()
//                .mapToInt(value -> value)
//                .sorted()
//                .boxed()
//                .collect(Collectors.toList());


        /**
         *     - podaj medianę elementów na liście posortowanej (wypisz ją)
         *     - znajdź największy i najmniejszy element na liście która nie była sortowana. Zrób to pętlą for
         *     - po znalezieniu elementu znajdź index elementu maksymalnego używając kolejnej pętli for
         *     - po znalezieniu elementów (największy i najmniejszy) znajdź index posługując się metodą indexof
         */
        // mediana to środkowa wartość w posortowanej kolekcji:

        System.out.println(kopia);
        if (kopia.size() % 2 == 1) { // w przypadku nieparzystej liczby elementów (mediana to środkowy)
            // 11 - (0-10)- size = 11/2 = 5 = mediana
            int srodkowyElement = kopia.get(kopia.size() / 2);

            System.out.println("Mediana: " + srodkowyElement);
        } else if (kopia.size() % 2 == 0) {
            // 10 - (0-9)- size = 10
            // (4 + 5 element) /2

            int srodkowyElement1 = kopia.get(kopia.size() / 2); // 10/2 = 5
            int srodkowyElement2 = kopia.get(kopia.size() / 2 - 1); // 10/2 -1 = 5-1 = 4
            System.out.println("Mediana: " + ((srodkowyElement1 + srodkowyElement2) / 2.0));
        }

        int max = list.get(0);
        int min = list.get(0); // muszę sprawdzić jeszcze n-1 elementów
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i) > max) {
                max = list.get(i);
            }
            if (list.get(i) < min) {
                min = list.get(i);
            }
        }
        System.out.println("Min :" +min);
        System.out.println("Index Min :" +list.indexOf(min)); // pierwsze wystąpienie

        System.out.println("Max :" +max);
        System.out.println("Index Max :" +list.indexOf(max));

        // TODO: zrobić to samo (indexof), tylko pętlą for.
    }
}
