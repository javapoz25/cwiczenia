package com.sda.javapoz25.listy.Zad5;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data               // getter, setter, toString, equals, hashcode
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private String nrIndeksu;
    private String imie, nazwisko;
    private Plec plec;

}
