package com.sda.javapoz25.listy.Zad5;

import java.util.ArrayList;
import java.util.List;

/**
 * ​Stwórz klasę Student która posiada pola:
 * nr indeksu
 * imie
 * nazwisko
 * płeć (wartość enum)
 * <p>
 * W mainie stwórz instancję kolekcji typu ArrayList, która zawiera obiekty klasy Student.Dodaj do kolekcji kilku studentów (dodaj je ręcznie - wpisz cokolwiek).
 * a. Spróbuj wypisać elementy listy stosując zwykłego "sout'a"
 * b. Spróbuj wypisać elementy stosując pętlę foreach
 * c. Wypisz tylko kobiety
 * d. Wypisz tylko mężczyzn
 * e. Wypisz tylko indeksy osób
 */
public class Main {
    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student("123", "Pawel", "Gawel", Plec.MEZCZYZNA));
        studentList.add(new Student("124", "Tosia", "Kowalska", Plec.KOBIETA));
        studentList.add(new Student("125", "Marek", "Kowalski", Plec.MEZCZYZNA));
        studentList.add(new Student("126", "Zosia", "Nowak", Plec.KOBIETA));
        studentList.add(new Student("127", "Ania", "Odrobina", Plec.KOBIETA));
        studentList.add(new Student("128", "Rafał", "Gawel", Plec.MEZCZYZNA));

        //a. Spróbuj wypisać elementy listy stosując zwykłego "sout'a"
        System.out.println("Lista:");
        System.out.println(studentList);

        //b. Spróbuj wypisać elementy stosując pętlę foreach
        System.out.println("Wszyscy:");
        for (Student student : studentList) {
            System.out.println(student);
        }

        //c. Wypisz tylko kobiety
        System.out.println("Kobiety:");
        for (Student student : studentList) {

            if (student.getPlec().equals(Plec.KOBIETA)) {
                System.out.println(student);
            }
        }

        //d. Wypisz tylko mężczyzn
        System.out.println("Mężczyźni:");
        for (Student student : studentList) {
            // po lewej stronie macie pewną wartość
            if (Plec.MEZCZYZNA.equals(student.getPlec())){
                System.out.println(student);
            }
        }

        //e. Wypisz tylko indeksy osób
        System.out.println("Indeksy:");
        for (Student student : studentList) {
            System.out.println(student.getNrIndeksu());
        }
    }
}
