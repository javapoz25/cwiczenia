package com.sda.javapoz25.listy.Zad3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Stwórz oddzielnego maina, a w nim kolejną listę ​String'ów​. Wykonaj zadania:
 * - dodaj do listy elementy 10030, 3004, 4000, 12355, 12222, 67888, 111200, 225355,2222, 1111, 3546, 138751, 235912
 * (jako stringi) (dodaj je posługując się metodąArrays.asList())
 * Przykład użycia Arrays.asList():
 * ArrayList<Integer> liczby = new ArrayList<>(Arrays.asList(23, 32, 44, 55, 677, 11, 33));
 * <p>
 * Podany przykład to tylko demonstracja metody Arrays.asList, zadanie należy wykonaćna liście String’ów.
 * - określ indeks elementu 138751 posługując się metodą indexOf
 * - sprawdź czy istnieje na liście obiekt 67888 metodą contains (wynik wypisz na ekran)
 * - sprawdź czy istnieje na liście obiekkt 67889 metodą contains (wynik wypisz na ekran)
 * - usuń z listy obiekt 67888 oraz 67889 (metoda remove)
 * - wykonaj ponownie powyższe sprawdzenia.
 * - iteruj całą listę, wypisz elementy na ekran (a. w jednej linii, b. każdy element woddzielnej linii).Sprawdź działanie aplikacji.
 */
public class Main {
    public static void main(String[] args) {
        // Arrays.asList - zwraca listę, której implementacja jest inna niż LinkedList i ArrayList
//        List<String> zle = Arrays.asList("10030", "3004", "4000", "12355", "12222", "67888", "111200", "225355", "2222", "1111", "3546", "138751", "235912");
//
//        System.out.println("Rozmiar: " + zle.size());
//        System.out.println("Element 1: "+ zle.get(0));
//        System.out.println("Element 5: "+ zle.get(4));
//
//        nie możemy dodawać/usuwac elementów
//        zle.add("cos");
//        zle.remove("10030");

        // dopisanie new ArrayList<>( spowoduje skopiowanie elementów do listy Arraylist)
        List<String> dobrze = new ArrayList<>(Arrays.asList("10030", "3004", "4000", "12355", "12222", "67888", "111200", "225355", "2222", "1111", "3546", "138751", "235912"));
        // jeśli znajdziemy element - indeks będzie dodatni - bo indeksujemy od 0 w górę
        // nie istnieje w liście indeks ujemny
        System.out.println("Indeks elementu 138751: " + dobrze.indexOf("138751"));     // jeśli nie znajdzie się indeks, wartość zwrócona to będzie -1

        System.out.println("Pierwsze sprawdzenie:");
        System.out.println("Istnienie elementu 67888: " + dobrze.contains("67888"));
        System.out.println("Istnienie elementu 67889: " + dobrze.contains("67889"));

//        System.out.println("Zle usuniecie elementu 67888: " + dobrze.remove(67888));
        System.out.println("Pierwsze usuniecie:");
        System.out.println("Dobre usuniecie elementu 67888: " + dobrze.remove("67888"));
        System.out.println("Usuniecie elementu 67889: " + dobrze.remove("67889"));

        System.out.println("Drugie sprawdzenie:");
        System.out.println("Istnienie elementu 67888: " + dobrze.contains("67888"));
        System.out.println("Istnienie elementu 67889: " + dobrze.contains("67889"));

//        - iteruj całą listę, wypisz elementy na ekran ( ).Sprawdź działanie aplikacji.
        System.out.println("Wypisania:");
        // (Zaznacz) (ctrl + alt + m)
        wypiszWJednejLinii(dobrze);

        wypiszWWieluLiniach(dobrze);
    }

    private static void wypiszWWieluLiniach(List<String> dobrze) {
        System.out.println();
//        b. każdy element woddzielnej linii
        for (String element : dobrze) {
            System.out.println(element);
        }
    }

    private static void wypiszWJednejLinii(List<String> dobrze) {
        System.out.println();
//        a. w jednej linii,
        StringBuilder sb = new StringBuilder("");
        for (String element : dobrze) {
            sb.append(element);
            sb.append(", ");
        }
        String wynik;
        if(dobrze.size() > 0){
            wynik = sb.substring(0, sb.length()-2); // ucinam dwa znaki
        }else{
            wynik = sb.toString();                  // jeśli nie ma żadnych elementów, to nie mogę obciąć znaków
                                                    // w tej sytuacji wyjdzie pusty string
        }
        System.out.println(wynik);
    }
}
