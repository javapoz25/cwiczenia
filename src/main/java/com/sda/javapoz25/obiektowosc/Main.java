package com.sda.javapoz25.obiektowosc;

public class Main {
    public static void main(String[] args) {
        double cenaNetto = 200;

        double cenaBrutto = cenaNetto + (cenaNetto*PodatekProduktu.VAT23.getWartoscPodatku());
        System.out.println(cenaBrutto); // 246
    }
}
