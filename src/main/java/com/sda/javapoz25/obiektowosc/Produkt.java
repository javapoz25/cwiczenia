package com.sda.javapoz25.obiektowosc;

public class Produkt {
    /*
    - nazwa produktu
    - cena produktu (netto)
    - ilość podatku (PodatekProduktu)
     */
    private String nazwa;
    private double cenaNetto;
    private PodatekProduktu podatekProduktu;
}
