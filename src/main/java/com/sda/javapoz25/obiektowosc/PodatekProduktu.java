package com.sda.javapoz25.obiektowosc;

//Tworzą się 4 obiekty, które posiadają dodatkową wartość - wartość podatku
public enum PodatekProduktu {
    VAT8(0.08),       // gdy użyje enum vat8, to 0.08 (8%)
    VAT23(0.23),      // -- || --                0.23 (23%)
    VAT5(0.05),
    NO_VAT(0.00);

    private double wartoscPodatku;

    PodatekProduktu(double wartoscPodatku) {
        this.wartoscPodatku = wartoscPodatku;
    }

    public double getWartoscPodatku() {
        return wartoscPodatku;
    }
    // bardzo błędne jest stworzenie settera
    // nie powinien istnieć setter do pola wewnątrz enum
    // enum z definicji jest stały
}
