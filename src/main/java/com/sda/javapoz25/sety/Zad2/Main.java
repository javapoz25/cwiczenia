package com.sda.javapoz25.sety.Zad2;

import java.util.*;

/**
 * Zadanie 1:Stwórz aplikację która przyjmuje od użytkownika ciąg znaków (dowolny). Podziel ciąg(split) na pojedyncze litery.
 * Twoim zadaniem jest stworzenie aplikacji która wypisze tylko unikalne litery frazy wpisanej przez użytkownika.
 * <p>
 * Pomyśl o wykorzystaniu cechy zbioru - pamiętaj, że zbiór sam usuwa duplikaty.
 * <p>
 * Zadanie 1b:Stwórz aplikację która dzieli zdanie na słowa, a następnie wypisuje tylko unikalne słowa
 * .*Dla chętnych zliczanie słów.
 * Wynikiem ma być Map<String, Long>
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj linię:");
        String linia = scanner.nextLine();

        String[] slowa = linia.split(" "); // podział na slowa

        // żeby nie kwalifikować spacji jako literki to usuniemy ją z linii
        linia = linia.replace(" ", "");     // opcjonalne
        String[] litery = linia.split(""); // podział na litery

        Set<String> literySet = new HashSet<>(Arrays.asList(litery));
        Set<String> slowaSet = new HashSet<>(Arrays.asList(slowa));

        System.out.println("Unikalne slowa: " + slowaSet);

        System.out.println("Duplikaty liter: " + (literySet.size() != litery.length));
        System.out.println("Duplikaty slow: " + (slowaSet.size() != slowa.length));

        Map<String, Long> zliczanieSlow = zliczanieSlowDoMapy(slowa);
        System.out.println(zliczanieSlow);
    }

    private static Map<String, Long> zliczanieSlowDoMapy(String[] slowa) {
        Map<String, Long> zliczanieSlow = new HashMap<>();
        // ala ale ala lubi
        for (String pojedynczeSlowo : slowa) {
            if (!zliczanieSlow.containsKey(pojedynczeSlowo)) {
                zliczanieSlow.put(pojedynczeSlowo, 1L); // wstawiam jedynkę, jeśli nie ma takiego slowa w mapie

            } else /*if (zliczanieSlow.containsKey(pojedynczeSlowo))*/ { // takie słowo znajduje się w mapie
                long ostatniaWartosc = zliczanieSlow.get(pojedynczeSlowo);

                zliczanieSlow.put(pojedynczeSlowo, ostatniaWartosc + 1);
            }
        }
        // stan mapy:
        // [
        //      'ala' : 2,
        //      'ale' : 1,
        //      'lubi' : 1,
        // ]
        return zliczanieSlow;
    }
}
