package com.sda.javapoz25.sety.Zad1;

import java.util.*;

/**
 * Ćwiczenia:1.Umieść wszystkie elementy tablicy {​10​,​12​,​10​,​3​,​4​,​12​,​12​,​300​,​12​,​40​,​55​} wzbiorze (HashSet) oraz:
 * Konstruktor kopiujący elementy listy do setu:
 * Set<Integer> liczby = ​new ​HashSet<>(Arrays.​asList​(​1​, ​2​ ... ));
 * 1.
 * a.Wypisz liczbę elementów na ekran (metoda size())
 * b.Wypisz wszystkie zbioru elementy na ekran (forEach)
 * c.Usuń elementy 10 i 12, wykonaj ponownie podpunkty a) i b)
 * d.zmień implementacje na TreeSet (zamień HashSet na TreeSet przy tworzeniuinstancji kolekcji)
 * <p>
 * 2.Napisz statyczną metodę sprawdzającą, czy w tekście nie powtarzają się litery zwykorzystaniem zbioru. (boolean zawieraDuplikaty(String text))
 * 3.Utwórz klasę ParaLiczb (int a,int b) i dodaj kilka instancji do zbioru:{(1,2), (2,1), (1,1), (1,2)}.
 * Wyświetl wszystkie elementy zbioru na ekran. Czy program działa zgodnie zoczekiwaniem?
 */
public class Main {
    public static void main(String[] args) {
        Integer[] tablica = new Integer[]{10, 12, 10, 3, 4, 12, 12, 300, 12, 40, 55};

        Set<Integer> zbior = new TreeSet<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return Integer.compare(o2, o1);
            }
        });
        zbior.addAll(Arrays.asList(tablica));

        System.out.println("Liczba el: " + zbior.size());
        System.out.println("Zbior : " + zbior);

        System.out.println("Uswanie 10: " + zbior.remove(10));
        System.out.println("Uswanie 12: " + zbior.remove(12));

        System.out.println("Liczba el: " + zbior.size());
        System.out.println("Zbior : " + zbior);

        System.out.println("Zawiera duplikaty: " + zawieraDuplikaty("ala"));
        System.out.println("Zawiera duplikaty: " + zawieraDuplikaty("kot"));

        Set<ParaLiczb> pary = new HashSet<>();
        pary.add(new ParaLiczb(1, 1));
        pary.add(new ParaLiczb(1, 2));
        pary.add(new ParaLiczb(2, 1));
        pary.add(new ParaLiczb(1, 2));

        System.out.println("Pary Liczb: " + pary.size());
        System.out.println("Pary: " + pary);
    }

    private static boolean zawieraDuplikaty(String text) {
        Set<Character> zbiorZnakow = new HashSet<>();
        for (int i = 0; i < text.length(); i++) {
            zbiorZnakow.add(text.charAt(i));            // dodaję każdą literę po kolei do zbioru
        }

        return zbiorZnakow.size() != text.length();
    }
}
