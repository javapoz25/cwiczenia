package com.sda.javapoz25.sety.Zad1;

import java.util.HashMap;
import java.util.Map;

public class MapaDuplikaty {
    public static void main(String[] args) {
        Map<String, Long> mapka = new HashMap<>();

        mapka.put("Slowo", 3L);
        mapka.put("Inne", 1L);
        mapka.put("Jeszcze", 5L);

        System.out.println(mapka);

        mapka.put("Inne", 120L);
        System.out.println(mapka);


    }
}
