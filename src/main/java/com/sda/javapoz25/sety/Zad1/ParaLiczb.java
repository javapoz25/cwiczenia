package com.sda.javapoz25.sety.Zad1;

import lombok.*;

import java.util.Objects;

@Getter
@Setter
//@EqualsAndHashCode
//@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ParaLiczb {
    private int a;
    private int b;

    // ponieważ nie została wygenerowana metoda hashcode, to set domyślnie porówna adresy w pamięci
    // można zastąpić adnotacją:
    // @EqualsAndHashCode
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParaLiczb paraLiczb = (ParaLiczb) o;
        return a == paraLiczb.a &&
                b == paraLiczb.b;
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b);
    }

    // Można zastąpić adnotacją:
    // @ToString
    @Override
    public String toString() {
        return "ParaLiczb{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }
}
